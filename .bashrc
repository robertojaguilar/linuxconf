if [ $SHELL != "/bin/bash" ]; then
	export SHELL=/bin/bash
	/bin/bash
	exit
fi

if [ -f /etc/bashrc ]; then
	source /etc/bashrc
fi

if [ -f /tools/bashrc-git ]; then
	source /tools/bashrc-git
fi

# include .bashrc_local if it exists
if [ -f $HOME/.bashrc_local ]; then
    . $HOME/.bashrc_local
fi

# tmux aliases
alias t='tmux'
alias ta='tmux a #'

# git aliases
alias g='git'
alias gbr='git branch'
alias gcm='git commit -m'
alias gca='git commit --amend'
alias gch='git checkout'
alias gst='git status'
alias glo='git log --decorate'
alias grb='git rebase'
alias gme='git merge'
alias grs='git reset'
alias grsh='git reset --hard'
alias gss='git stash save'
alias gsp='git stash pop'
alias gph='git push'
alias gpl='git pull'
alias grl='git reflog'
alias gdi='git diff'
alias grt='git remote -v'
alias grv='git review'
alias gad='git add'
alias gau='git add -u'
alias grm='git rm'
alias gsh='git show'
alias gfe='git fetch'

# other aliases
alias grn='grep -rn'
alias cl='clear'
alias fn='find -name'
alias la='ls -A'
alias v='vim'

# make aliases
alias mda='make devenv_add'
alias mdi='make devenv_init'
alias mdr='make devenv_rm'
alias mdc='make distclean'
alias mcl='make clean'
alias m='make'
